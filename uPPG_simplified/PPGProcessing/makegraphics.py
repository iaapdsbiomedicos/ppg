#!/usr/bin/env python3

## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## PROFESSORS: Cesar Augusto Prior and Cesar Rodrigues (Yeah. Its almost an overflow!)
## PROJECT: µPPG - Photoplethysmography waves acquisition
## ARCHIVE: Plot ppg signal script
## DATE: 18/11/2018 - updated @ 15/03/2019
## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

import matplotlib.pyplot as plt
from PPGProcessing.readandfilter import *


# GRAPHICAL FUNCTIONS -----------------------------------------------------

# Plot PPG raw signals
def plotRawSignals():
    plt.figure('Noisy and filtered PPG signals from RED and IR', figsize=(14,6)) # 20,10

    plt.subplot(2,2,1)
    plt.title("Raw RED signal")
    plt.ylabel("amplitude (v)")
    plt.plot(x, RED, "red")
    plt.grid()

    plt.subplot(2,2,2)
    plt.title("Raw IR signal")
    plt.plot(x, IR, "orange")
    plt.grid()

    plt.subplot(2,1,2)
    plt.title("Filtered Signals")
    plt.xlabel("time (s)")
    plt.ylabel("amplitude (v)")
    plt.plot(x, REDf, "red", label="RED Signal")
    plt.plot(x, IRf, "orange", label="IR Signal")
    plt.legend(loc="best")
    plt.grid()

    plt.show()
#end-def