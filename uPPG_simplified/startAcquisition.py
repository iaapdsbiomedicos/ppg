#!/usr/bin/env python3

## +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
## DEVELOPER: Cesar Abascal
## PROFESSORS: Cesar Augusto Prior and Cesar Rodrigues (Yeah. Its almost an overflow!)
## PROJECT: µPPG - Photoplethysmography waves acquisition
## ARCHIVE: Simple loop infinite acquisition main page
## DATE: 18/11/2018 - updated @ 15/03/2019
## ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


## LIBRARIES ----------------------------------------------------------------------------
import os
from PPGAcquisition.AFEcore import functions as f


## MAIN --------------------------------------------------------------------------------
try:
    # Show uPPG screen on display and info on terminal
    f.showInitInfos()

    # Get pacient name and create txt file to save data
    f.patientN = str(input("Patient name: "))
    path = "ppg-data/raw-ppg/" + f.patientN
    if(not os.path.exists(path)):
        os.makedirs(path)
    f.dataFile = open(path+"/PPG_signals.csv", "w+")
    f.dataFile.write("REDsignal;IRsignal;synced\n")

    # Open SPI connection
    f.openSPIConnection()

    # Initialize AFE4490 CI
    f.AFEinit()

    # Show acquiring screen on display and info on terminal
    f.showAcqInfos()

    # Set Interruption Service Routine
    f.setISR()

    # Start acquisition loop until ctrl-c be pressed
    f.mainLoop()
#end-try

except KeyboardInterrupt:
    # Stop and finish acquisition when crtl-c where pressed
    f.finish()
#end-except